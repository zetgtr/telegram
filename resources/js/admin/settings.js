class Settings {
    constructor() {
        this.select = document.getElementById('chat_select')
        this.remove = document.getElementById('chat_remove')
        this.routeRemove = document.getElementById('route_remove').value
        this.id = document.getElementById('chat_id')
        this.comment = document.getElementById('chat_comment')
        this.add = document.getElementById('chat_add')
        this.routeAdd = document.getElementById('route_add').value
        this.chat = document.getElementById('chat')
        this.sand = document.getElementById('chat_sand')
        this.routeSand = document.getElementById('route_sand').value
        this.addEvent()
    }

    addEvent(){
        this.add.addEventListener('click',this.chatAdd.bind(this))
        this.remove.addEventListener('click',this.chatRemove.bind(this))
        this.sand.addEventListener('click',this.chatSand.bind(this))
    }

    chatAdd(){
        axios.post(this.routeAdd,{chat_id:this.id.value, comment:this.comment.value}).then(()=>window.location.reload())
    }

    chatRemove(){
        const selectedValues = Array.from(this.select.selectedOptions, option => option.value);
        if(confirm('Вы точно хотите удалить?'))
            axios.post(this.routeRemove,{chats:selectedValues}).then(()=>window.location.reload())
    }

    chatSand(){
        axios.post(this.routeSand,{message:this.chat.value}).then(()=>window.location.reload())
    }
}

$(document).ready(()=>{
    new Settings()
})

