@extends('layouts.admin')
@section('title',config('telegram.title'))
@section('content')
    <div class="card">
        <div class="card-header card-header-divider">
            <div>
                <h3 class="card-title">Настройки бота</h3>
            </div>
        </div>
        <div class="card-body">
            <x-warning />
            <div class="row">
                <form action="{{ route('admin.telegram.save') }}" method="POST" class="col-lg-4">
                    @csrf
                    <div class="form-group">
                        <div class="form-group">
                            <label for="link">Ссылка на чат</label>
                            <input class="form-control" type="text" name="link" value="{{ $telegram?->link }}" id="link">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="token">Токен <i class="fad fa-question-circle" data-bs-toggle="modal" data-bs-target="#info_token" style="cursor: pointer;color: #1170e4"></i></label>
                            <input class="form-control" type="text" name="token" value="{{ $telegram?->token }}" id="token">
                            <button class="btn btn-sm btn-primary mt-2" id="chat_save">Сохранить</button>
                        </div>
                    </div>
                </form>
                @if($telegram)
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="chat_id">Добавить чат <i class="fad fa-question-circle" data-bs-toggle="modal" data-bs-target="#info_chat" style="cursor: pointer;color: #1170e4"></i></label>
                            <div class="d-flex">
                                <input class="form-control" type="text" id="chat_id" placeholder="Чат id">
                                <div class="mx-2"></div>
                                <input type="text" class="form-control" id="chat_comment" placeholder="Комментарий">
                            </div>
                            <button class="btn btn-sm btn-success mt-2" id="chat_add">Добавить</button>
                        </div>
                        <div class="form-group">
                            <label for="chat_id">Список чатов</label>
                            <select class="form-control" id="chat_select" multiple="">
                                @foreach($chats as $chat)
                                    <option value="{{ $chat->chat_id }}">чат: {{ $chat->chat_id }}{{ $chat->comment ? ', '.$chat->comment : '' }}</option>
                                @endforeach

                            </select>
                            <div class="w-100 d-flex justify-content-start">
                                <button class="btn btn-sm btn-danger mt-2" id="chat_remove">Удалить чат</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="chat">Проверка чата </label>
                                <textarea class="form-control chat" type="text" id="chat"></textarea>
                                <div class="w-100 d-flex justify-content-start">
                                    <button class="btn btn-sm btn-success mt-2" id="chat_sand">Отправить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <input type="hidden" id="route_add" value="{{ route('admin.telegram.add') }}">
    <input type="hidden" id="route_remove" value="{{ route('admin.telegram.remove') }}">
    <input type="hidden" id="route_sand" value="{{ route('admin.telegram.sand') }}">
    <x-telegram::modals.info_token />
    <x-telegram::modals.info_chat :token="$telegram?->token" />
    @vite('resources/js/telegram/admin/settings.js')
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item active"><a href="{{route("admin.telegram.index")}}">{{ config('telegram.title') }}</a></li>
        </ol>
    </div>
@endsection
