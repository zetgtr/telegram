<div class="modal fade" id="info_token">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">Инструкция по получению токена Telegram бота</h6>
                <button aria-label="Close" class="btn-close" data-bs-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <p class="text-center">Чтобы создать Telegram Bot API, нужно найти BotFather в Телеграме. Это отец всех ботов. С помощью
                        него можно создавать и управлять многочисленными чат-ботами.&nbsp;</p>
                    <p>Инструкция, как получить токен в Телеграм:</p>
                    <ol class="p-4">
                        <li class="mb-2">Вбить в поисковом поле мессенджера <a
                                target="_blank" href="https://t.me/BotFather">@BotFather</a>.<img
                                src="/storage/telegram/token/1.jpg" alt="Введите: php artisan vendor:publish --tag=telegram_iamges" style="width: 355px;" class="fr-fic fr-dib"></li>
                        <li class="mb-2">Активировать найденную утилиту. Это делают следующей командой «/start».<img
                                src="/storage/telegram/token/2.jpg" alt="Введите: php artisan vendor:publish --tag=telegram_iamges" style="width: 651px;" class="fr-fic fr-dib"></li>
                        <li class="mb-2">Откроется окно, где пользователю нужно найти фразу «/new bot». Кликнуть по ней.<img
                                src="/storage/telegram/token/3.jpg" alt="Введите: php artisan vendor:publish --tag=telegram_iamges" style="width: 651px;" class="fr-fic fr-dib"></li>
                        <li class="mb-2">Дать имя будущему роботу, который будет общаться с клиентами.<img
                                src="/storage/telegram/token/4.jpg" alt="Введите: php artisan vendor:publish --tag=telegram_iamges" style="width: 651px;" class="fr-fic fr-dib"></li>
                        <li class="mb-2">Если юзернейм бота будет занят, то система сообщит об этом.<img
                                src="/storage/telegram/token/5.jpg" alt="Введите: php artisan vendor:publish --tag=telegram_iamges" style="width: 651px;" class="fr-fic fr-dib"></li>
                        <li class="mb-2">Нужно хорошо подумать и прописать еще раз имя. Следует не забывать после названия вводить
                            «_bot». Иначе система не примет название.
                        </li>
                        <li>После того, как будет создано оригинальное название, система выдаст API токен.<img
                                src="/storage/telegram/token/6.jpg" style="width: 651px;" class="fr-fic fr-dib"></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>


