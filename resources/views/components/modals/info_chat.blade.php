<div class="modal fade" id="info_chat">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">Инструкция по получению Chat ID</h6>
                <button aria-label="Close" class="btn-close" data-bs-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <p>Чтобы найти чат ID пользователя, необходимо зайти в телеграм и нажать кнопку «Запустить», далее написать @get_id_bot.&nbsp;</p>
                    <p>Также можно перейти по <a target="_blank" href="https://api.telegram.org/bot{{$token ? $token : ''}}/getUpdates">ссылке</a> и найти нужный ID, предварительно отправив соощение боту.</p>
                </div>
            </div>
        </div>
    </div>
</div>


