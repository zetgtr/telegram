<?php

namespace Contact\Enums;

enum MenuEnums: string
{
    case INFO = "info";
    case SETTINGS = "settings";

    public static function all(): array
    {
        return [
            self::INFO->value,
            self::SETTINGS->value
        ];
    }
}
