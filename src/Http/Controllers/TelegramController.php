<?php

namespace Telegram\Http\Controllers;


use App\Models\Admin\Catalog\Filter;
use App\Http\Controllers\Controller;
use Telegram\Models\Telegram;
use Telegram\Models\TelegramChat;
use Telegram\QueryBuilder\TelegramBuilder;
use Illuminate\Http\Request;
use Telegram\Requests\Admin\TelegramChatAddRequest;
use Telegram\Requests\Admin\TelegramChatRemoveRequest;
use Telegram\Requests\Admin\TelegramMessageRequest;
use Telegram\Requests\Admin\TelegramSaveRequest;

class TelegramController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('telegram::admin.index',['telegram'=>Telegram::first(),'chats'=>TelegramChat::get()]);
    }

    public function save(TelegramSaveRequest $request)
    {
        Telegram::updateOrCreate(['id'=>1],$request->validated());
        return redirect()->back()->with('success','Успешно обновлено');
    }

    public function add(TelegramChatAddRequest $request)
    {
        TelegramChat::create($request->validated());
        return redirect()->back()->with('success','Успешно обновлено');
    }
    public function remove(TelegramChatRemoveRequest $request)
    {
        foreach ($request->chats as $chat){
            TelegramChat::where('chat_id',$chat)->delete();
        }
        return redirect()->back()->with('success','Успешно обновлено');
    }

    public function sand(TelegramMessageRequest $request,TelegramBuilder $builder){
        $builder->sendMessage($request->message);
        return ['status'=>true];
    }
}
