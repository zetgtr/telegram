<?php

namespace Telegram\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TelegramSaveRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'link' => 'nullable',
            'token' => 'required',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
