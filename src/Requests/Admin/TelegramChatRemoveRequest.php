<?php

namespace Telegram\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TelegramChatRemoveRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'chats' => 'array|required'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
