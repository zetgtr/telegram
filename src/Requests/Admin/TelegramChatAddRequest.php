<?php

namespace Telegram\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TelegramChatAddRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'telegram_id' => 'required',
            'chat_id' => 'required|unique:telegram_chats,chat_id',
            'comment' => 'nullable',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'telegram_id' => 1,
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
