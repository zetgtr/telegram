<?php

namespace Telegram\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TelegramMessageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'message'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
