<?php

namespace Telegram\Providers;

use Illuminate\Support\ServiceProvider as Provider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class ServiceProvider extends Provider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        }

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'telegram');

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/../../config/telegram.php' => config_path('telegram.php'),
        ], 'telegram_config');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/telegram'),
        ], 'telegram_views');

        $this->publishes([
            __DIR__.'/../../resources/img' => storage_path('app/public/telegram'),
        ], 'telegram_iamges');

        $this->publishes([
            __DIR__.'/../../resources/js' => resource_path('js/telegram'),
        ], 'telegram_scripts');
//
//
//        $this->publishes([
//            __DIR__.'/../../resources/scss' => resource_path('assets/quest/quest'),
//        ], 'quest_scss');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'telegram');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'telegram');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
    }

    private function components()
    {
//        Blade::component(\Quest\View\Admin\Settings::class, 'quest::quest.settings');
//        Blade::component(\quest\View\Admin\Icons::class, 'quest::type.icons');
//        Blade::component(\Quest\View\Admin\Type::class, 'quest::type.type');
    }

    private function singletons()
    {

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(QueryBuilder::class, CatalogBuilder::class);
        $this->mergeConfigFrom(__DIR__ . '/../../config/telegram.php', 'telegram');
        $this->singletons();
    }
}
