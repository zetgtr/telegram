<?php

namespace Telegram\Models;

use Illuminate\Database\Eloquent\Model;

class TelegramChat extends Model
{
    protected $fillable = [
        'telegram_id',
        'comment',
        'chat_id',
    ];
}
