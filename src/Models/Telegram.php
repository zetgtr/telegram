<?php

namespace Telegram\Models;

use Illuminate\Database\Eloquent\Model;

class Telegram extends Model
{
    protected $table = 'telegram';

    protected $fillable = [
        'link',
        'token',
    ];
    
    public function chats(){
        return $this->hasMany(TelegramChat::class,'telegram_id','id');
    }
}
