<?php

namespace Telegram\QueryBuilder;


use Telegram\Models\Telegram;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\HttpException;

class TelegramBuilder
{
    public function __construct()
    {
        $this->telegram = Telegram::first();
        $this->bot = new BotApi($this->telegram->token);
    }

    public function sendMessage($message){
        try{
            $chats = $this->telegram->chats;
            foreach ($chats as $chat){
                $check = $this->bot->getChat($chat->chat_id);
                if ($check) {
                    $this->bot->sendMessage($chat->chat_id, $message, 'Markdown');
                }
            }
        }catch (HttpException  $e){
        }
    }
}
