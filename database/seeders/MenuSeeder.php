<?php

namespace Telegram\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>config('telegram.database_id'),'name'=>config('telegram.title'),'position'=>'right','logo'=>'fab fa-telegram-plane','controller'=>'Telegram\Http\Controllers\ContactController','url'=>'telegram','parent'=>3, 'order'=>7],
        ];
    }
}
