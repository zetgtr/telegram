<?php

namespace Telegram\Remove;

use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RemovePackage
{
    private $pathMigration;
    private $pathScript;
    public function __construct()
    {
        $this->pathMigration = database_path('migrations');
        $this->pathScript = resource_path('js/telegram');
    }

    private function deleteMigration($files){
        $check = false;
        foreach ($files as $file) {
            try {
                $path = $this->pathMigration."/".$file->getFilename();
                if (File::exists($path)) {
                    Artisan::call('migrate:rollback --path=database/migrations/'.$file->getFilename());
                    unlink($path);
                }
            }catch (\Exception $exception){
                $check = true;
            }
        }
        if($check){
            $this->deleteMigration($files);
        }
    }


    public function run($settings,$migration = false, $script = false, $vies = false)
    {
        chdir(base_path());
        if($migration)
        {
            $directory = __DIR__ . "/../database/migrations";
            $files = File::files($directory);
            $this->deleteMigration($files);



            $menu = Menu::query()->find(config('telegram.database_id'));
            if($menu)
                $menu->delete();
        }


        if (File::isDirectory($this->pathScript))
            File::deleteDirectory($this->pathScript);

    }
}
