<?php


use Illuminate\Support\Facades\Route;
use Telegram\Http\Controllers\TelegramController;



Route::middleware('web')->group(function (){
   Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => ['menu.check:977','is_admin']],static function() {
       Route::group(['prefix'=>"telegram", 'as'=>'telegram.'],static function() {
           Route::get('/', [TelegramController::class, 'index'])->name('index');
           Route::post('/save', [TelegramController::class, 'save'])->name('save');
           Route::post('/add', [TelegramController::class, 'add'])->name('add');
           Route::post('/remove', [TelegramController::class, 'remove'])->name('remove');
           Route::post('/sand', [TelegramController::class, 'sand'])->name('sand');
       });
   });
});

